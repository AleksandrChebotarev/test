import React from "react";
import { List } from "../List";

export const Wrapper = ({users, handleDelete}) => {
  return (users ? (
    <List users={users} handleDelete={handleDelete}/>
  ) : (
    <h2>У вас нет пользователей в списке</h2>
  ))
};
