import React from "react";
import { Button } from "../Button";
export const ListItem = ({ id, name, phone, website, email, company, handleDelete }) => {
    return (
    <div>
      <div>
        <h3>{name}</h3>
        <p>
          <span>Phone</span>
          {phone}
        </p>
        <p>
          <span>website</span>
          {website}
        </p>
        <p>
          <span>email</span>
          {email}
        </p>
        <p>
          <span>company</span>
          {company}
        </p>
      </div>
      <Button onClick={()=>handleDelete(id)} className={"list-item-btn"}>Удалить из списка</Button>
    </div>
  );
};
