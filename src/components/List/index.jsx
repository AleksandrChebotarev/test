import React from "react";
import { ListItem } from "../ListItem";

export const List = ({ users,handleDelete }) => {
  return users.map((user) => (
    <ListItem key={user.id} user={user} handleDelete={handleDelete}/>
  ));
};
