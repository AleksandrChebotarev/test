import React, { useEffect, useState } from 'react';
import './App.css';
import { Wrapper } from './components/Wrapper';
import { deleteItem } from './utils';

const App=()=> {

  const [data, setData] = useState([]);

  useEffect(()=>{
    fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(json => setData(json))
  },[])

  const handleDelete = (id) => {
    setData(deleteItem(data, id));
  };

  return (
    <div className="App">
      <Wrapper users={data} handleDelete={handleDelete}/>
    </div>
  );
}

export default App;
